
require('dotenv').config()

const debug = require('debug')('loader');

const uuid = require('uuid/v1')
const AWS = require('aws-sdk')
const fs = require('fs');
const os = require('os');

AWS.config.getCredentials((err)=>{
  if(err) console.log(`credentials error ${err}`)
});

AWS.config.update({region:'us-east-1'});

const s3 = new AWS.S3({apiVersion:'2006-03-01'});

const DL_DIR = `${os.homedir()}\\Downloads`;

const getPath = (fileName) => {
  return `${DL_DIR}\\${fileName}`;
}

const s3Upload = async (file) => {
  debug(`file ${file}`)
  const stream = fs.createReadStream(file)
    s3.upload({
      Bucket: process.env.BUCKET,
      Key: `uploads/dance_party_${uuid().toString()}.mp4`,
      Body: stream,
      ContentType: 'video/mp4',
      ACL: 'public-read'
    }, (awsError, uploadResponse) => {
      if (awsError) {
        debug(`awsError: `, awsError)
        throw new Error(awsError);
      }
      console.log('Successfully Uploaded', uploadResponse)
    });
};

setInterval(() => {
  const dirOutput = fs.readdirSync(DL_DIR, 'utf8')
  const dirMovieStats = dirOutput
    .filter(file => file.endsWith('.mp4'))
    .map(movieFile => ({name: movieFile, birthTime:fs.statSync(getPath(movieFile)).birthtimeMs}));

  if (dirMovieStats.length) {
    const files = dirMovieStats.sort((a, b) => b.birthTime - a.birthTime);
    const toUpload = files[0]
    const file = getPath(toUpload.name)

    console.log(`Will upload ${file}`)
    s3Upload(file)
      .then(() => {
        fs.unlinkSync(file)
      })
      .catch((error) => {
        console.log('ahhhhh error', error);
      })

  } else {
    console.log('No videos found');
  }
}, 6000)




